/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ezjamvc.view.beans;
import com.ezjamvc.modelo.delegate.EzjaMVCDelegate;
import com.ezjamvc.modelo.dto.ArticuloDTO;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
/* @author Asuncion */
@ManagedBean(name = "ArticuloBean")
@SessionScoped
public class ArticuloBean extends BaseBean {
        private ArticuloDTO alumnoDTO;
        public ArticuloBean() {
        }
        public String nuevo() {
                alumnoDTO = new ArticuloDTO();
                setAccion(ACC_CREAR);
                return "/articulos/capturarArticulo.xhtml";
        }
        public String editar() {
                setAccion(ACC_ACTUALIZAR);
                return "/articulos/capturarArticulo.xhtml";
        }
        public String crear() {
                EzjaMVCDelegate escuelaDelegate = new EzjaMVCDelegate();
                try {
                        escuelaDelegate.crearArticulo(alumnoDTO);
                        return "/articulos/listadoArticulo.xhtml";
                } catch (Exception e) {
                        error("errorCrearArticulo", "Error al crear articulo");
                        return "/articulos/listadoArticulo.xhtml";
                }
        }
        public String actualizar() {
                EzjaMVCDelegate escuelaDelegate = new EzjaMVCDelegate();
                try {
                        escuelaDelegate.actualiza(alumnoDTO);
                        return "/articulos/listadoArticulo.xhtml";
                } catch (Exception e) {
                        error("errorCrearAlumno", "Error al crear articulo");
                        return "/articulos/listadoArticulo.xhtml";
                }
        }
        public String borrar() {
                EzjaMVCDelegate escuelaDelegate = new EzjaMVCDelegate();
                try {
                        escuelaDelegate.elimina(alumnoDTO);
                } catch (Exception ex) {
                        ex.printStackTrace();
                }
                return "/articulos/listadoArticulo.xhtml";
        }
        public List getLista() {
                EzjaMVCDelegate escuelaDelegate = new EzjaMVCDelegate();
                try {
                        return escuelaDelegate.listarArticulos();
                } catch (Exception e) {
                        e.printStackTrace();
                        error("ErrorListaArticulos", "Error al mostrar articulos");
                        return null;
                }
        }
        public void seleccionaArticulo(ActionEvent event) {
                EzjaMVCDelegate escuelaDelegate = new EzjaMVCDelegate();
                String claveArtSel = (String) FacesContext.getCurrentInstance()
                                .getExternalContext().getRequestParameterMap()
                                .get("claveArtSel");
                alumnoDTO = new ArticuloDTO();
                alumnoDTO.setClaveArticulo(claveArtSel);
                try {
                        alumnoDTO = escuelaDelegate.leerArticulo(alumnoDTO);
                } catch (Exception ex) {
                        ex.printStackTrace();
                }
        }
        public String getClaveArticulo() {
                return alumnoDTO.getClaveArticulo();
        }
        public void setClaveArticulo(String claveArticulo) {
                alumnoDTO.setClaveArticulo(claveArticulo);
        }
        public String getDescripcion() {
                return alumnoDTO.getDescripcion();
        }
        public void setDescripcion(String descripcion) {
                alumnoDTO.setDescripcion(descripcion);
        }
        public int getExistencias() {
                return alumnoDTO.getExistencias();
        }
        public void setExistencias(int existencias) {
                alumnoDTO.setExistencias(existencias);
        }
        public double getPrecio() {
                return alumnoDTO.getPrecio();
        }
        public void setPrecio(double precio) {
                alumnoDTO.setPrecio(precio);
        }
}