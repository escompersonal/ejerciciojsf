/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ezjamvc.modelo.dto;

/**
 *
 * @author alumno
 */
public class ArticuloDTO {
    private String claveArticulo;
    private String descripcion;
    private double precio;
    private int existencias;

    public ArticuloDTO() {
    }

    public String getClaveArticulo() {
        return claveArticulo;
    }

    public void setClaveArticulo(String claveArticulo) {
        this.claveArticulo = claveArticulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getExistencias() {
        return existencias;
    }

    public void setExistencias(int existencias) {
        this.existencias = existencias;
    }
    
       @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Clave Articulo ").append(getClaveArticulo()).append("\n");
        sb.append("Descripcion Articulo ").append(getDescripcion()).append("\n");
        sb.append("Precio Articulo ").append(getPrecio()).append("\n");
        sb.append("Existencias Articulo ").append(getExistencias()).append("\n");
        return sb.toString();
    }
    
    
}
