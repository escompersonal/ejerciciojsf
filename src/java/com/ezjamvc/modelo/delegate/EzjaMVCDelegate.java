/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ezjamvc.modelo.delegate;

import com.ezjamvc.modelo.dto.ArticuloDTO;
import com.ezjamvc.modelo.facade.ArticuloFacade;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author alumno
 */
public class EzjaMVCDelegate {
    private Connection cnn;
    private ArticuloFacade artFacade;
    public EzjaMVCDelegate() {
        String user = "root";
        String pwd = "root";
        String url = "jdbc:mysql://localhost:3306/EzjaMVC";
        String mySqlDriver = "com.mysql.jdbc.Driver";
        try {
            Class.forName(mySqlDriver);
            cnn = DriverManager.getConnection(url, user, pwd);
        } catch (Exception e) {
            e.printStackTrace();
        }
        artFacade = new ArticuloFacade(cnn);
    }
    //Codigo para los Articulos
    public void crearArticulo(ArticuloDTO dto) throws SQLException {
        artFacade.crear(dto);
    }
    public List listarArticulos() throws SQLException {
        return artFacade.listar();
    }
    public ArticuloDTO leerArticulo(ArticuloDTO dto) throws SQLException {
        return artFacade.leer(dto);
    }
    public void actualiza(ArticuloDTO dto) throws SQLException {
        artFacade.actualiza(dto);
    }
    public void elimina(ArticuloDTO dto) throws SQLException {
        artFacade.elimina(dto);
    }
}
